##
# Project Title
#
# @file
# @version 0.1

cc = gcc -Wall -lm
files = ./src/main.c ./src/token.c ./src/stack.c ./src/logger.c ./src/grammer.c ./src/dict.c
out = ./main

main: $(files)
	$(cc) $(files) -o $(out)

run: main
	$(out)

val: main
	valgrind $(out)
# end
