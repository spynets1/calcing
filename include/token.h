#include <stdbool.h>

#ifndef TOKEN_H_
#define TOKEN_H_

enum tokens { value, operator, openbracket, closingbracket };

typedef struct {
  enum tokens token;
  char *value;
} Token;

Token *token_new(enum tokens token, char *value);
void add_token(Token **tokens, int *t_count, enum tokens token, char *c);
int tokenize(Token **tokens, int *tcount, char *input);
bool check_grammer(Token **tokens, int count);

bool can_be_operator(char *input);
bool is_operator(char *input);
bool is_integer(char input);

int get_priority(char *token);

void token_free(Token *token);

#endif // TOKEN_H_
