

typedef struct node {
  struct node *next;
  void *value;
} Node;

typedef struct stack {
  Node *top;
  int capacity;
  int size;
} Stack;

void stack_push(Stack *stack, void *node);

/** void* means value
 * */
void *stack_pop(Stack *stack);

Stack *stack_new();
void stack_free(Stack *stack);
