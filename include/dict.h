#ifndef DICT_H_
#define DICT_H_

typedef struct dict_node {
  struct dict_node *next;
  void *data;
  char *key;
} dict_node;

typedef struct {
  struct dict_node **list;
  unsigned int size;
} Dict;

Dict *dict_new();
void dict_add(Dict *dict, const char *key, void *data,
              unsigned int size_of_data);
void *dict_get(Dict *dict, const char *key);
void dict_free(Dict *dict);

#endif // DICT_H_
