#include "../include/dict.h";
#include "../include/logger.h"
#include "../include/stack.h"
#include "../include/token.h"
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int to_postfix(Token ***output, int *t_count, char *input) {
  bool should_log = false;

  Token **tokens = malloc(sizeof(Token *) * 256);

  // if the tokenizer had an error we return -1
  if (tokenize(tokens, t_count, input) == -1) {
    // free all tokens
    for (int i = 0; i < *t_count; i++) {
      token_free(tokens[i]);
    }
    free(tokens);
    return -1;
  }

  if (!check_grammer(tokens, *t_count)) {
    puts("\nWrong syntax");
    exit(1);
  }

  (*output) = malloc(sizeof(Token *) * (*t_count));
  int o_count = 0;
  Stack *operator_stack = stack_new();

  for (int i = 0; i < *t_count; i++) {
    Token *curr = tokens[i];
    if (curr->token == value) {
      logingln(should_log, "add value to the output");
      (*output)[o_count] = curr;
      o_count++;
    }
    if (curr->token == openbracket) {
      stack_push(operator_stack, curr);
    }
    // when finding a closing bracket we pop all operators untill
    // we find a openbracket
    if (curr->token == closingbracket) {
      while (operator_stack->size > 0 &&
             strcmp(((Token *)operator_stack->top->value)->value, "(") != 0) {
        Token *poped = stack_pop(operator_stack);
        loging(should_log, "the new operator has lower priority so we pop %s\n",
               poped->value);
        (*output)[o_count] = poped;
        o_count++;
      }
      token_free(stack_pop(operator_stack));
      token_free(curr);
    }

    if (curr->token == operator) {
      int new_p = get_priority(curr->value);
      while (operator_stack->size > 0 &&
             new_p <
                 get_priority(((Token *)operator_stack->top->value)->value)) {
        logingln(should_log, "the new operator has lower priority so we pop");
        new_p = get_priority(curr->value);
        (*output)[o_count] = stack_pop(operator_stack);
        o_count++;
      }
      stack_push(operator_stack, curr);
    }
  }
  while (operator_stack->size > 0) {
    (*output)[o_count] = stack_pop(operator_stack);
    o_count++;
  }

  for (int i = 0; i < o_count; i++) {
    printf("%s ", (*output)[i]->value);
  }
  puts("");

  stack_free(operator_stack);

  free(tokens);

  (*t_count) = o_count;
  return 0;
}

double calculate_postfix(Token **input, int t_count) {
  Stack *stack = stack_new();
  for (int i = 0; i < t_count; i++) {
    char *val = input[i]->value;
    if (is_operator(val)) {
      double *a = stack_pop(stack);
      double *b = stack_pop(stack);
      double *s = malloc(sizeof(double));

      if (a == NULL ||
          (b == NULL && strcmp(val, "-") != 0 && strcmp(val, "+") != 0)) {
        free(s);
        stack_free(stack);
        return 0; // Error: insufficient operands
      }

      if (strcmp(val, "-") == 0) {
        *s = (b == NULL) ? -*a : *b - *a;
      } else if (strcmp(val, "+") == 0) {
        *s = (b == NULL) ? *a : *b + *a;
      } else if (strcmp(val, "*") == 0) {
        *s = *b * *a;
      } else if (strcmp(val, "/") == 0) {
        *s = *b / *a;
      } else if (strcmp(val, "**") == 0 || strcmp(val, "^") == 0) {
        *s = pow(*b, *a);
      } else {
        free(s);
        stack_free(stack);
        return 0;
      }

      stack_push(stack, s);
      free(a);
      if (b != NULL) {
        free(b);
      }
    } else {
      double *v = malloc(sizeof(double));
      *v = atof(val);
      stack_push(stack, v);
    }
  }

  double *s = stack_pop(stack);
  if (s == NULL) {
    stack_free(stack);
    return 0; // Error: no result
  }

  double result = *s;
  free(s);
  stack_free(stack);
  return result;
}

double calculate(char *input) {
  Token **tokens;
  int count = 0;
  if (to_postfix(&tokens, &count, input) == -1)
    return 0;

  double ans = calculate_postfix(tokens, count);

  for (int i = 0; i < count; i++) {
    token_free(tokens[i]);
  }
  free(tokens);
  return ans;
}

char *replace_substring(const char *str, const char *old_sub,
                        const char *new_sub) {
  const char *pos;
  int count = 0;
  int old_len = strlen(old_sub);
  int new_len = strlen(new_sub);

  // Count the number of occurrences of the old substring
  for (pos = str; (pos = strstr(pos, old_sub)) != NULL; pos += old_len) {
    count++;
  }

  // Allocate memory for the new string
  size_t new_str_len = strlen(str) + count * (new_len - old_len) + 1;
  char *new_str = (char *)malloc(new_str_len);
  if (!new_str) {
    return NULL; // Memory allocation failed
  }

  // Replace occurrences of the old substring with the new substring
  char *new_str_pos = new_str;
  const char *old_str_pos = str;
  while ((pos = strstr(old_str_pos, old_sub)) != NULL) {
    size_t len_up_to_sub = pos - old_str_pos;
    memcpy(new_str_pos, old_str_pos, len_up_to_sub);
    new_str_pos += len_up_to_sub;
    memcpy(new_str_pos, new_sub, new_len);
    new_str_pos += new_len;
    old_str_pos = pos + old_len;
  }

  // Copy the remainder of the original string
  strcpy(new_str_pos, old_str_pos);

  return new_str;
}

void replace_variables(Dict *variables, char *input) {
  for (int i = 0; i < 10; i++) {
    dict_node *tmp = variables->list[i];
    while (tmp != NULL) {
      char data[sizeof(double) + 1];
      sprintf(data, "%lf", *(double *)tmp->data);
      strcpy(input, replace_substring(input, tmp->key, data));
      tmp = tmp->next;
    }
  }
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char **split_string(const char *str, const char *delimiter, int *count) {
  char *str_copy =
      strdup(str); // Duplicate the input string for safe tokenizing
  if (str_copy == NULL) {
    return NULL; // Memory allocation failed
  }

  // Count the number of tokens
  int token_count = 0;
  char *temp_str = strdup(str); // Duplicate again for counting
  char *token = strtok(temp_str, delimiter);
  while (token != NULL) {
    token_count++;
    token = strtok(NULL, delimiter);
  }
  free(temp_str);

  // Allocate memory for the token array
  char **tokens = (char **)malloc((token_count + 1) * sizeof(char *));
  if (tokens == NULL) {
    free(str_copy);
    return NULL; // Memory allocation failed
  }

  // Tokenize the string and fill the token array
  token_count = 0;
  token = strtok(str_copy, delimiter);
  while (token != NULL) {
    tokens[token_count++] = strdup(token); // Duplicate each token
    token = strtok(NULL, delimiter);
  }
  tokens[token_count] = NULL; // Null-terminate the array

  *count = token_count;
  free(str_copy);
  return tokens;
}

void free_strings(char **tokens, int count) {
  for (int i = 0; i < count; i++) {
    free(tokens[i]);
  }
  free(tokens);
}

int set_variable(Dict *variables, char *input) {
  puts(input);
  if (strstr(input, "=")) {
    int count = 0;
    char **result = split_string(input, "=", &count);
    if (result != NULL) {
      for (int i = 0; i < count - 1; i++) {
        replace_variables(variables, result[i + 1]);
        double ans = calculate(result[i + 1]);
        printf("%lf\n> ", ans);
        dict_add(variables, result[i], &ans, sizeof(ans));
      }
      free_strings(result, count);
    } else {
      printf("Memory allocation failed\n");
    }
    return 1;
  }
  return 0;
}

int main() {
  Dict *dict = dict_new();
  double pi = 3.14;
  dict_add(dict, "pi", &pi, sizeof(double));

  char input[256];
  double ans = 0;
  printf("> ");
  scanf("%s", input);
  while (strcmp(input, "exit") != 0 && strcmp(input, "q") != 0) {
    if (set_variable(dict, input) == 1) {
      scanf("%s", input);
      continue;
    }
    replace_variables(dict, input);

    puts(input);
    ans = calculate(input);
    dict_add(dict, "ans", &ans, sizeof(double));

    printf("%lf\n> ", ans);
    scanf("%s", input);
  }

  dict_free(dict);

  return 0;
}
