#include "../include/grammer.h"
#include "../include/token.h"
#include <stdbool.h>

bool perenthesis_test(Token **tokens, int n) {
  int count = 0;
  for (int i = 0; i < n; i++) {
    if (tokens[i]->token == openbracket)
      count++;
    if (tokens[i]->token == closingbracket)
      count--;
  }
  return true ? count == 0 : false;
}
bool check_grammer(Token **tokens, int count) {
  if (!perenthesis_test(tokens, count))
    return false;

  if (tokens[0]->token == operator)
    return false;
  if (tokens[count - 1]->token == operator)
    return false;

  for (int i = 0; i < count - 1; i++) {
    if (tokens[i]->token == value && tokens[i + 1]->token == value)
      return false;
    if (tokens[i]->token == operator&& tokens[i + 1]->token == operator)
      return false;
    if (tokens[i]->token == openbracket &&
        tokens[i + 1]->token == closingbracket)
      return false;
    if (tokens[i]->token == closingbracket &&
        tokens[i + 1]->token == openbracket)
      return false;
  }
  return true;
}
