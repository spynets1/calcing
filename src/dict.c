#include "../include/dict.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Dict *dict_new() {
  Dict *new = malloc(sizeof(Dict));
  new->list = malloc(sizeof(dict_node) * 10);
  for (int i = 0; i < 10; i++) {
    new->list[i] = NULL;
  }
  new->size = 0;
  return new;
}

int get_index(const char *key) {
  int sum = 0;
  for (int i = 0; i < strlen(key); i++) {
    sum += (int)key[i];
  }
  return sum % 10;
}

void dict_node_free(dict_node *node) {
  free(node->data);
  free(node->key);
  free(node);
}

void dict_add_node(dict_node **start, const char *key, void *data,
                   unsigned int size_of_data) {
  dict_node *new = malloc(sizeof(dict_node));
  new->data = malloc(size_of_data);
  memcpy(new->data, data, size_of_data);
  new->key = malloc(sizeof(char) * strlen(key) + 1);
  strcpy(new->key, key);
  new->next = NULL;

  if (*start == NULL) {
    *start = new;
    return;
  }
  dict_node *tmp = *start;
  while (tmp != NULL) {
    if (strcmp(tmp->key, key) == 0) {
      free(tmp->data);
      tmp->data = malloc(size_of_data);
      memcpy(tmp->data, data, size_of_data);
      tmp->next = NULL;
      dict_node_free(new);
      return;
    }
    tmp = tmp->next;
  }
  tmp->next = new;
}

void dict_add(Dict *dict, const char *key, void *data,
              unsigned int size_of_data) {
  int index = get_index(key);
  dict_add_node(&dict->list[index], key, data, size_of_data);
}
void *dict_get(Dict *dict, const char *key) {
  int index = get_index(key);
  dict_node *node = dict->list[index];
  return dict->list[index]->data;
}
void dict_free(Dict *dict) {
  for (int i = 0; i < 10; i++) {
    dict_node *tmp = dict->list[i];
    while (tmp != NULL) {
      dict_node *fretmp = tmp;
      tmp = tmp->next;
      dict_node_free(fretmp);
    }
  }
  free(dict->list);
  free(dict);
}
