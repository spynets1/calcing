#include "../include/token.h"
#include "../include/logger.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *operators[] = {"-", "+", "*", "/", "**", "^"};
int op_count = 6;
int priority[] = {0, 1, 2, 3, 4, 5};

const char *token_names[] = {"VALUE", "OPERATOR", "OPENBRACKET",
                             "CLOSINGBRACKET"};

bool should_log = false;

// returns the proprity of the operator
// speciefed by the priority array
int get_priority(char *token) {
  for (int i = 0; i < op_count; i++) {
    if (strcmp(operators[i], token) == 0)
      return priority[i];
  }
  return -1;
}

Token *token_new(enum tokens token, char *value) {
  Token *new = malloc(sizeof(Token));
  new->token = token;
  new->value = malloc(sizeof(char) * strlen(value) + 1);
  strcpy(new->value, value);
  return new;
}

void add_token(Token **tokens, int *t_count, enum tokens token, char *c) {
  loging(should_log, "add token %s\n", c);
  tokens[*t_count] = token_new(token, c);
  (*t_count)++;
}
void append_char(char *reading, size_t size, char c) {
  size_t len = strlen(reading);
  if (len + 1 < size) {
    snprintf(reading + len, size - len, "%c", c);
  } else {
    // Handle the error: the buffer is too small to append the character
    fprintf(stderr, "Buffer overflow prevented\n");
  }
}

int tokenize(Token **tokens, int *tcount, char *input) {
  int t_count = 0;

  char reading[256];
  memset(reading, '\0', 256);
  for (int i = 0; i < strlen(input); i++) {
    char c = input[i];
    char cs[2];
    sprintf(cs, "%c", c);
    loging(should_log, "%d READING: %c\n", i, c);

    if (c == '(') {
      if (strlen(reading) > 0)
        add_token(tokens, &t_count, value, reading);
      reading[0] = 0;

      // if there was no operator before the bracket
      // add a multiplication token
      char tmp[2];
      sprintf(tmp, "%c", input[i - 1]);
      if (can_be_operator(tmp) != 1) {
        add_token(tokens, &t_count, operator, "*");
      }

      add_token(tokens, &t_count, openbracket, "(");
    } else if (c == ')') {
      if (strlen(reading) > 0)
        add_token(tokens, &t_count, value, reading);
      reading[0] = 0;
      add_token(tokens, &t_count, closingbracket, ")");
      // if there was no operator before the bracket
      // add a multiplication token
      char tmp[2];
      sprintf(tmp, "%c", input[i + 1]);
      if (i + 1 < strlen(input) && can_be_operator(tmp) != 1) {
        add_token(tokens, &t_count, operator, "*");
      }

    } else if (c == ' ') {
      if (strlen(reading) > 0)
        add_token(tokens, &t_count, value, reading);
      reading[0] = 0;

    } else if (can_be_operator(cs)) {
      // add token value before opeartor
      if (strlen(reading) > 0)
        add_token(tokens, &t_count, value, reading);
      // temp variable holding the reading before new character
      char tmp[256];
      // reseting the reading with the first char
      sprintf(reading, "%c", c);
      // while current string is a operator add the next charecter
      // untill it is no nlonger a opeartor
      // then add the operator with the temp value
      // subtract one from i so we are back next
      // for loop is correct
      while (can_be_operator(reading)) {
        loging(should_log, "Tmp %s\n", reading);
        // get the next char and
        c = input[++i];
        // save old reading
        sprintf(tmp, "%s", reading);
        // set new reading
        append_char(reading, sizeof(reading), c);
      }
      // check if the value we read is really a operator
      if (is_operator(tmp)) {
        if (strcmp(tmp, "-") == 0 &&
            (t_count == 0 || tokens[t_count - 1]->token != value)) {
          puts("neg");
          add_token(tokens, &t_count, value, "0");
        }
        add_token(tokens, &t_count, operator, tmp);
      }

      i--;
      // reset reading
      reading[0] = 0;
    } else if (is_integer(c)) {
      append_char(reading, sizeof(reading), c);
    } else {

      printf("Character '%c' not recognized\n", c);
      return -1;
    }
  }
  // add the last reading
  if (strlen(reading) > 0)
    add_token(tokens, &t_count, value, reading);

  for (int i = 0; should_log && i < t_count; i++) {
    loging(should_log, "%s ", token_names[tokens[i]->token]);
  }

  for (int i = 0; i < t_count; i++) {
    printf("%s ", token_names[tokens[i]->token]);
  }
  puts("");

  *tcount = t_count;
  return 0;
}

bool can_be_operator(char *input) {
  for (int i = 0; i < op_count; i++) {
    if (strstr(operators[i], input) != NULL) {
      return true; // Match found
    }
  }
  return false;
}
bool is_operator(char *input) {
  for (int i = 0; i < op_count; i++) {
    if (strcmp(operators[i], input) == 0) {
      return true; // Match found
    }
  }
  return false;
}

bool is_integer(char input) {
  char num[2];
  sprintf(num, "%c", input);
  if (atoi(num) == 0 && strcmp(num, "0") != false && strcmp(num, ".") != 0)
    return false;
  return true;
}

void token_free(Token *token) {
  free(token->value);
  free(token);
}
