#include "../include/stack.h"
#include <stdlib.h>
#include <string.h>

Stack *stack_new() {
  Stack *new = malloc(sizeof(Stack));
  new->top = NULL;
  new->size = 0;
  return new;
}

void stack_push(Stack *stack, void *value) {
  Node *node = malloc(sizeof(Node));
  node->value = value;
  node->next = stack->top;
  stack->top = node;
  stack->size++;
}

void *stack_pop(Stack *stack) {
  if (stack->top == NULL) {
    return NULL;
  }
  Node *node = stack->top;
  stack->top = stack->top->next;
  stack->size--;
  void *pop = node->value;
  free(node);
  return pop;
}

void stack_free(Stack *stack) {
  while (stack->size > 0) {
    stack_pop(stack);
  }
  free(stack);
}
