#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "../include/logger.h"

void loging(bool loging, const char *format, ...) {
    if(loging == true){
        va_list args;
        va_start(args, format);
        vprintf(format, args);
        va_end(args);
    }
}

void logingln(bool loging, const char *format, ...) {
    if(loging == true){
        va_list args;
        va_start(args, format);
        vprintf(format, args);
        puts("");
        va_end(args);
    }
}
